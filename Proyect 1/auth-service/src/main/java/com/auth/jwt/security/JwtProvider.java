package com.auth.jwt.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Component
public class JwtProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtProvider.class);
    private static final String SECRET_KEY = "123";
    private static final Algorithm ALGORITHM = Algorithm.HMAC256(SECRET_KEY);

    public String create(String username) {
        String token = JWT.create()
                .withSubject(username)
                .withIssuer("123")
                .withIssuedAt(new Date())
                .withExpiresAt(new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(15)))
                .sign(ALGORITHM);

        logger.info("Se ha creado un token JWT para el usuario '{}'", username);
        return token;
    }

    public boolean isValid(String jwt) {
        try {
            JWT.require(ALGORITHM)
                    .build()
                    .verify(jwt);
            logger.info("El token JWT es válido");
            return true;
        } catch (JWTVerificationException e) {
            logger.error("Error al verificar la validez del token JWT: {}", e.getMessage());
            return false;
        }
    }

    public String getUserName(String jwt) {
        try {
            String username = JWT.require(ALGORITHM)
                    .build()
                    .verify(jwt)
                    .getSubject();
            logger.info("Se ha obtenido el nombre de usuario '{}' del token JWT", username);
            return username;
        } catch (JWTVerificationException e) {
            logger.error("Error al obtener el nombre de usuario del token JWT: {}", e.getMessage());
            return null;
        }
    }
}
