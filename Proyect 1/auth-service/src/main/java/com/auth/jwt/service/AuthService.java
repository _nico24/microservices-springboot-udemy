package com.auth.jwt.service;

import com.auth.jwt.dto.AuthUserDto;
import com.auth.jwt.dto.TokenDto;
import com.auth.jwt.entity.AuthUser;
import com.auth.jwt.repository.AuthUserRepository;
import com.auth.jwt.security.JwtProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

@Service
public class AuthService {

    private static final Logger logger = LoggerFactory.getLogger(AuthService.class);


    @Autowired
    private AuthUserRepository authUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtProvider jwtProvider;

    public AuthUser save(AuthUserDto dto) {
        Optional<AuthUser> user = authUserRepository.findByUserName(dto.getUserName());
        if (user.isPresent()) {
            logger.warn("El usuario '{}' ya existe en la base de datos", dto.getUserName());
            return null;
        }
        String password = passwordEncoder.encode(dto.getPassword());
        AuthUser authUser = AuthUser.builder()
                .userName(dto.getUserName())
                .password(password)
                .build();
        authUser = authUserRepository.save(authUser);
        if (authUser != null) {
            logger.info("Usuario '{}' creado correctamente", authUser.getUserName());
        } else {
            logger.error("Error al crear el usuario '{}'", dto.getUserName());
        }
        return authUser;
    }

    public TokenDto login(AuthUserDto dto) {
        Optional<AuthUser> user = authUserRepository.findByUserName(dto.getUserName());
        if (!user.isPresent()) {
            return null;
        }
        if (passwordEncoder.matches(dto.getPassword(), user.get().getPassword())) {
            return new TokenDto(jwtProvider.create(String.valueOf(user.get())));
        }
        return null;
    }

    public TokenDto validate(String token) {
        if (!jwtProvider.isValid(token)) {
            return null;
        }
        String userName = jwtProvider.getUserName(token);
        if (!authUserRepository.findByUserName(userName).isPresent()) {
            return null;
        }
        return new TokenDto(token);
    }
}
