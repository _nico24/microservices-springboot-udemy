package com.calification.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@SpringBootApplication
@EnableDiscoveryClient
public class CalificationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalificationServiceApplication.class, args);
	}

}
