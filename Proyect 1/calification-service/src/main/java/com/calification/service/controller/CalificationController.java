package com.calification.service.controller;


import com.calification.service.entity.Calification;
import com.calification.service.service.CalificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/califications")
public class CalificationController {

    @Autowired
    private CalificationService calificationService;


    @PostMapping
    public ResponseEntity<Calification> saveCalification(@RequestBody Calification calification){
        return  ResponseEntity.status(HttpStatus.CREATED).body(calificationService.create(calification));
    }

    @GetMapping
    public ResponseEntity<List<Calification>> listCalifications(){
        return ResponseEntity.ok(calificationService.getCalifications());
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<List<Calification>> listCalificationByUserId(@PathVariable String userId){
        return ResponseEntity.ok(calificationService.getCalificationsByUserId(userId));
    }


    @GetMapping("/hotels/{hotelId}")
    public ResponseEntity<List<Calification>> listCalificationsByHotelId(@PathVariable String hotelId){
        return ResponseEntity.ok(calificationService.getCalificationsByHotelId(hotelId));
    }

}
