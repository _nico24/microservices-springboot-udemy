package com.calification.service.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document("califications")
public class Calification {
    @Id
    private String calificationId;

    private String userId;

    private String hotelId;

    private int calification;

    private String observations;
}
