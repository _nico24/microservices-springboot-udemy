package com.calification.service.repository;

import com.calification.service.entity.Calification;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CalificationRepository extends MongoRepository<Calification, Long> {

    List<Calification> findByUserId(String userId);
    List<Calification> findByHotelId(String hotelId);
}
