package com.calification.service.service;

import com.calification.service.entity.Calification;

import java.util.List;

public interface CalificationService {

    Calification create(Calification calification);

    List<Calification> getCalifications();
    List<Calification> getCalificationsByUserId(String userId);
    List<Calification> getCalificationsByHotelId(String hotelId);


}
