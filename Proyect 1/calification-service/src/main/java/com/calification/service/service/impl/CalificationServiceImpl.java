package com.calification.service.service.impl;

import com.calification.service.entity.Calification;
import com.calification.service.repository.CalificationRepository;
import com.calification.service.service.CalificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CalificationServiceImpl implements CalificationService {

    @Autowired
    private CalificationRepository calificationRepository;

    @Override
    public Calification create(Calification calification) {
        return calificationRepository.save(calification);
    }

    @Override
    public List<Calification> getCalifications() {
        return calificationRepository.findAll();
    }

    @Override
    public List<Calification> getCalificationsByUserId(String userId) {
        return calificationRepository.findByUserId(userId);
    }

    @Override
    public List<Calification> getCalificationsByHotelId(String hotelId) {
        return calificationRepository.findByHotelId(hotelId);
    }
}
