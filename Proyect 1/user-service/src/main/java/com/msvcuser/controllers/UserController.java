package com.msvcuser.controllers;

import com.msvcuser.entity.User;
import com.msvcuser.service.UserService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<User> saveUser(@RequestBody User userRequest){
        User user = userService.saveUser(userRequest);
        return ResponseEntity.status(HttpStatus.CREATED).body(user);
    }

    @GetMapping
    public ResponseEntity<List<User>> listUsers(){
        List<User> users = userService.getAllUsers();
        return ResponseEntity.ok(users);
    }

    int numberOfAttempts = 1;
    @GetMapping("/{userId}")
    //@CircuitBreaker(name = "ratingHotelBreaker",fallbackMethod = "ratingHotelFallback")
    @Retry(name = "ratingHotelService", fallbackMethod = "ratingHotelFallback")
    public ResponseEntity<User> obtainUser(@PathVariable String userId){
        log.info("List a single user : UserController");
        log.info("Number of retries : {}", numberOfAttempts);
        numberOfAttempts ++;
        User user = userService.getUser(userId);
        return  ResponseEntity.ok(user);
    }

    public ResponseEntity<User> ratingHotelFallback(String userId, Exception exception){
        log.info("The backup runs because the service is inactive: ", exception.getMessage());
        User user = User.builder()
                .email("root@gmail.com")
                .name("root")
                .information("This user is created by default when a service crashes")
                .userId("1234")
                .build();
        return new ResponseEntity<>(user, HttpStatus.OK);
    }


}
