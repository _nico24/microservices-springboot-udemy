package com.msvcuser.service;

import com.msvcuser.entity.User;

import java.util.List;

public interface UserService {
    User saveUser(User user);

    List<User> getAllUsers();

    User getUser(String userId);

}
