package com.msvcuser.service.impl;

import com.msvcuser.entity.Calification;
import com.msvcuser.entity.Hotel;
import com.msvcuser.entity.User;
import com.msvcuser.exceptions.ResourceNotFoundException;
import com.msvcuser.external.services.HotelService;
import com.msvcuser.repository.UserRepository;
import com.msvcuser.service.UserService;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HotelService hotelService;

    @Override
    public User saveUser(User user) {
        String ramdonUserId = UUID.randomUUID().toString();
        user.setUserId(ramdonUserId);
        return userRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUser(String userId) {
        User user = userRepository.findById(userId).orElseThrow(
                () -> new ResourceNotFoundException("user not found with ID : " + userId));

        Calification[] calificationOfUser = restTemplate.getForObject("http://CALIFICATION-SERVICE/califications/users/" + user.getUserId(), Calification[].class);
        logger.info("{}", calificationOfUser);
        List<Calification> califications = Arrays.stream(calificationOfUser).collect(Collectors.toList());
        List<Calification> calificationList = califications.stream().map(calification -> {
            System.out.println(calification.getHotelId());
            Hotel hotel = hotelService.getHotel(calification.getHotelId());
            calification.setHotel(hotel);
            return calification;
        }).collect(Collectors.toList());
        user.setCalifications(calificationList);
        return user;
    }
}
